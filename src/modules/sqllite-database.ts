import log from "@ajar/marker";
import sqlite3 from "sqlite3";
import { ITask } from "./interfaces";

export default class sqliteDB {
    private _DBpath = "./default.db";
    //private _db : any;

    constructor(DBpath: string) {
        this._DBpath = DBpath;
    }

    public init = async (): Promise<boolean> => {
        try {
            const db = await new sqlite3.Database(this._DBpath, sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE);
            await db.run("CREATE TABLE tasks(id,title,isActive)");
            db.close();
            return true;
        } catch (err) {
            log.err("Failed to init DB: ", err);
            return false;
        }
    };

    public write = async (data: ITask): Promise<boolean> => {
        try {
            const db = new sqlite3.Database(this._DBpath);
            await db.run("INSERT INTO tasks(id,title,isActive) VALUES(?,?,?)", [
                data.id,
                data.title,
                data.isActive,
            ]);
            db.close();
            return true;
        } catch (err) {
            log.err("Failed to add DB entry: ", err);
            return false;
        }
    };

    public read = async (): Promise<any> => {
        // try {
        //     const data = await fs.readFile(this._DBpath, "utf-8");
        //     return JSON.parse(data);
        // } catch (err) {
        //     // log.err(`Failed to read from '${this._DBpath}'`);
        //     return [];
        // }
        return true;
    };

    //     public erase = async (): Promise<boolean> => {
    //         try {
    //             await fs.unlink(this._DBpath);
    //             return true;
    //         } catch (error) {
    //             log.err(`Failed to delete from '${this._DBpath}'`);
    //             return false;
    //         }
    //     }
}
