export interface ICommand {
    title: string;
    arguments: IArguments;
}

export interface IArguments {
    [key: string]: string;
}

export interface IHelpInfo {
    description: string;
    parameters: string;
    examples: string;
}
export interface IHelpObj {
    [key: string]: IHelpInfo;
}

export interface ITask {
    id: string;
    title: string;
    isActive: string | boolean;
}
